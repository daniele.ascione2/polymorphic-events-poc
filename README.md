# Polymorphic Events POC

## Prerequisites

- Docker and Docker Compose
- IntelliJ plugin [EnvFile](https://plugins.jetbrains.com/plugin/7861-envfile)

### To play the demo

- Run in your terminal:
  `docker-compose up -d`
- Generate the Kotlin classes:
    ```shell
    ./gradlew event-schemas:generateKotlinClassesFromAvro
    ```
- Run from your IDE in any order:
    - The producer `ErrorSender`
    - The producer `SuccessSender`
- Run, using your IDE the application `App`
    - If you're using IntelliJ, it should detect the folder `.run` with the config to start the app. And if you've got the EnvFile plugin installed, it should
      populate the right env variables for Stream Machine, getting them from the `.env` file.

If all was successful, App should print out:

```
key-883b67d3-836b-4da3-bf11-203ccc08e70f: SuccessEvent(eventId=d74dfde3-d6c7-4aca-a0e2-6997faf76dea, traceId=49b87287-2d7d-47cb-81d8-9d80f62a129a)
Received SuccessEvent: SuccessEvent(eventId=d74dfde3-d6c7-4aca-a0e2-6997faf76dea, traceId=49b87287-2d7d-47cb-81d8-9d80f62a129a)
key-77c09f70-24e0-4af7-9e51-04fa4f06d547: ErrorEvent(eventId=b72426cb-4613-42fe-a9ca-5b6ec7b8ddff, traceId=b327c850-347d-4817-9ed5-df38f24438f6)
Received ErrorEvent: ErrorEvent(eventId=b72426cb-4613-42fe-a9ca-5b6ec7b8ddff, traceId=b327c850-347d-4817-9ed5-df38f24438f6)
```

#### What happens

The producers will register the schemas:

1. `PolymorphicFacts-com.masabi.justride.examples.Success`

```avro schema
{
    "type": "record",
    "name": "Success",
    "namespace": "com.masabi.justride.examples",
    "fields": [
        {
            "name": "traceId",
            "type": {
                "type": "string",
                "logicalType": "uuid"
            }
        },
        {
            "name": "eventId",
            "type": {
                "type": "string",
                "logicalType": "uuid"
            }
        }
    ]
}
```

2. `PolymorphicFacts-com.masabi.justride.examples.ErrorEvent`

```avro schema
{
    "type": "record",
    "name": "ErrorEvent",
    "namespace": "com.masabi.justride.examples",
    "fields": [
        {
            "name": "eventId",
            "type": {
                "type": "string",
                "logicalType": "uuid"
            }
        },
        {
            "name": "traceId",
            "type": {
                "type": "string",
                "logicalType": "uuid"
            }
        }
    ]
}
```

If you executed the `docker-compose up -d` command, you can inspect the schemas using [Kafka UI](http://localhost:8080/ui/clusters/local/schemas).

- The producers will send messages to the topic, that is bounded to a single schema.

**Note**
As we're in "dev mode", the producers run with `auto.register.schemas=true`, which means that the schema registry will not perform any compatibility check. The
producer will always override the latest version of the schema for that subject.

Not something that we want in a production environment, but not even UAT. Read more about
it [from the article that Ioannis sent](https://www.confluent.io/blog/multiple-event-types-in-the-same-kafka-topic/).

- The `App` will read the message and cast it to appropriate type:
  ```kotlin
  val v: Any
  when (v) {
    is SuccessEvent -> println("Received SuccessEvent: $v")
    is ErrorEvent -> println("Received ErrorEvent: $v")
    else -> println("Received unknown type: ${v::class}")
  }
  ```

#### Follow up questions and considerations

A table that compares the naming strategies can be
found [here](https://docs.confluent.io/platform/current/schema-registry/serdes-develop/index.html#how-the-naming-strategies-work).

1. The producer, when it registers a schema, will always override the latest schema, without performing any compatibility checks. Hence, we need something that
   registers the schema and runs on a CD Pipeline, and at the same time ensuring that all the clients have the following configs:
   ```
   auto.register.schemas=false 
   use.latest.version=true
   ```
   Note:
    1. there might
       be [an issue in avro-4k serializer, that could prevent us from using `use.latest.schema=true`](https://github.com/thake/avro4k-kafka-serializer/issues).
       Hopefully before we go to prod this is solved (either by us or by the owner of the repo).
    2. With a fork of Kafka gitops that uses [this PR](https://github.com/devshawn/kafka-gitops/pull/76) we could register the schemas, but we'd need to convert
       the avpr/avdl to
       avsc. [Avro-tools (see `IdlToSchermataTool)](https://github.com/apache/avro/blob/master/lang/java/tools/src/main/java/org/apache/avro/tool/Main.java#L42)
       can help with that.
2. If we want to have a standardization for the events platform-wise and not topic-wise, arguably the best option that we have is to use
   the `RecordNameStrategy`. Deciding which strategy to use requires some thinking, because in some cases it's desirable to evolve schemas at topic level (and
   this is where `TopicRecordNameStrategy` suits better). For a comparison table see this doc
   from [Confluent](https://docs.confluent.io/platform/current/schema-registry/serdes-develop/index.html#group-by-topic-or-other-relationships).
3. From what I can see avro-4k serializer (or at least [avro4k-compiler](https://github.com/thake/avro4k-compiler/issues/100)) does not support unions with
   sealed classes, which would be a very useful feature to have compile-type guarantee that every event type is handled properly.

   But that would imply that we'd use explicit unions in the Avro schema definition:
   ```avro idl
     record MyRecordType {

     import idl "../common/MySuccessType.avdl";
     import idl "../common/MyFailureType.avdl";

      @logicalType("uuid") string traceId;   
      @logicalType("uuid") string eventId;   
   
      union {
         com.masabi.justride.MySuccessType,
         com.masabi.justride.MyFailureType
      } payload;                                 
   }
   ```
   Imagine how good it would be to do something like:
   ```kotlin
   val event: MySealedInterface = consumerEvent().payload
   when (event) {
       is MySuccessType -> processSuccess()
       is MyFailureType -> processFailure()
   }
   ```
   and get a compile-time error if I forget to handle a specific type!

   In this way we could keep using the default `value.subject.name.strategy`, but it seems the path that requires more work on the plugin (gpak) and
   avro-4k-serializer side.

   
