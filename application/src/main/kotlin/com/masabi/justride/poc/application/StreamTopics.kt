package com.masabi.justride.poc.application

import com.masabi.justride.streammachine.StreamTopic

object StreamTopics {

    val POLYMORPHIC_FACTS = StreamTopic<Any>("PolymorphicFacts")
    val TEST = StreamTopic<Any>("Test")
}