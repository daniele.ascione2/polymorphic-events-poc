package com.masabi.justride.poc.application

object App {

    @JvmStatic
    fun main(args: Array<String>) {
        StreamApp(applicationId = System.getenv("APPLICATION_ID")).start()
    }
}