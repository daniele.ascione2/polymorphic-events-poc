package com.masabi.justride.poc.application

import com.masabi.justride.examples.ErrorEvent
import com.masabi.justride.examples.SuccessEvent
import com.masabi.justride.streammachine.KafkaStreamsProcessor
import com.masabi.justride.streammachine.StreamMachine
import com.masabi.justride.streammachine.StreamProcessor
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology

class StreamApp(
    private val applicationId: String,
    private val streamProcessor: StreamProcessor = KafkaStreamsProcessor()
) {

    fun start() =
        StreamMachine(
            applicationId = applicationId,
            topology = createTopology(),
            streamProcessor = streamProcessor
        ).start()

    private fun createTopology(): Topology =
        mapRawScansToAvro().build()

    private fun mapRawScansToAvro() =
        StreamsBuilder()
            .apply {
                stream<String, Any>(StreamTopics.POLYMORPHIC_FACTS.topicName)
                    .foreach { k, v ->
                        println("$k: $v")

                        when (v) {
                            is SuccessEvent -> println("Received SuccessEvent: $v")
                            is ErrorEvent -> println("Received ErrorEvent: $v")
                            else -> println("Received unknown type: ${v::class}")
                        }
                    }
            }
}