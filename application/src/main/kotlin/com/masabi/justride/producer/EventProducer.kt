package com.masabi.justride.producer

import com.github.thake.kafka.avro4k.serializer.KafkaAvro4kSerializer
import io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig
import io.confluent.kafka.serializers.subject.TopicRecordNameStrategy
import org.apache.kafka.clients.CommonClientConfigs
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.clients.producer.RecordMetadata
import org.apache.kafka.common.serialization.StringSerializer
import java.util.concurrent.Future


class EventProducer<T> {

    val producerConfig = mapOf(
        CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG to "localhost:29092",

        ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG to StringSerializer::class.java.name,
        ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG to KafkaAvro4kSerializer::class.java.name,

        AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG to "http://localhost:8081",
        AbstractKafkaSchemaSerDeConfig.AUTO_REGISTER_SCHEMAS to true,
        AbstractKafkaSchemaSerDeConfig.VALUE_SUBJECT_NAME_STRATEGY to TopicRecordNameStrategy::class.java.name,
    )

    private val kafkaProducer: KafkaProducer<String, T> = KafkaProducer(producerConfig)

    fun sendMessage(topic: String, key: String, value: T) {
        kafkaProducer.sendAndPrint(ProducerRecord(topic, key, value))
        kafkaProducer.flush()
    }

    private fun <V> KafkaProducer<String, V>.sendAndPrint(record: ProducerRecord<String, V>): Future<RecordMetadata> =
        send(record) { m: RecordMetadata, e: Exception? ->
            when (e) {
                null -> println("Produced message: ${record.value()} to topic ${m.topic()} partition [${m.partition()}] @ offset ${m.offset()}")
                else -> println("Exception: $e")
            }
        }
}


