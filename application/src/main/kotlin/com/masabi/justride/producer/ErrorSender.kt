package com.masabi.justride.producer

import com.masabi.justride.examples.ErrorEvent
import com.masabi.justride.poc.application.StreamTopics.POLYMORPHIC_FACTS
import java.util.UUID

object ErrorSender {

    @JvmStatic
    fun main(args: Array<String>) {
        EventProducer<ErrorEvent>()
            .sendMessage(
                POLYMORPHIC_FACTS.topicName,
                "key-${UUID.randomUUID()}",
                ErrorEvent(traceId = UUID.randomUUID(), eventId = UUID.randomUUID())
            )
    }
}