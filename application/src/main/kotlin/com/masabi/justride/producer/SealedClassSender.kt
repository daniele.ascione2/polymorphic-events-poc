package com.masabi.justride.producer

import com.masabi.justride.poc.application.StreamTopics.TEST
import kotlinx.serialization.Serializable
import java.util.UUID


// FIXME: Tested to experiment with sealed classes. It fails in serialization.
object SealedClassSender {

    @JvmStatic
    fun main(args: Array<String>) {
        EventProducer<MyEvent>()
            .sendMessage(
                TEST.topicName,
                "key-${UUID.randomUUID()}",
                Failure(reason = "I hate Avro")
            )
    }
}

sealed class MyEvent(
    open val eventId: UUID = UUID.randomUUID(),
    open val traceId: UUID = UUID.randomUUID(),
)

@Serializable
data class Failure(
    @Serializable(com.github.avrokotlin.avro4k.serializer.UUIDSerializer::class)
    override val eventId: UUID = UUID.randomUUID(),
    @Serializable(com.github.avrokotlin.avro4k.serializer.UUIDSerializer::class)
    override val traceId: UUID = UUID.randomUUID(),
    @Serializable
    val reason: String,
) : MyEvent(eventId, traceId)

@Serializable
data class Success(
    @Serializable(com.github.avrokotlin.avro4k.serializer.UUIDSerializer::class)
    override val eventId: UUID = UUID.randomUUID(),
    @Serializable(com.github.avrokotlin.avro4k.serializer.UUIDSerializer::class)
    override val traceId: UUID = UUID.randomUUID(),
) : MyEvent(eventId, traceId)

