package com.masabi.justride.producer

import com.masabi.justride.examples.SuccessEvent
import com.masabi.justride.poc.application.StreamTopics.POLYMORPHIC_FACTS
import java.util.UUID

object SuccessSender {

    @JvmStatic
    fun main(args: Array<String>) {
        EventProducer<SuccessEvent>()
            .sendMessage(
                POLYMORPHIC_FACTS.topicName,
                "key-${UUID.randomUUID()}",
                SuccessEvent(traceId = UUID.randomUUID(), eventId = UUID.randomUUID())
            )
    }
}