//class ScanMapperAcceptanceTest {
//
//    val testDriverStreamProcessor = TestDriverStreamProcessor()
//
//    val application = StreamApp("test", testDriverStreamProcessor)
//
//    val inputTopic by lazy {
//        testDriverStreamProcessor.testDriver.createInputTopic(
//            StreamTopics.COMMANDS.topicName, StringSerializer(), StringSerializer())
//    }
//    val outputTopic by lazy { testDriverStreamProcessor.createOutputTopic(StreamTopics.POLYMORPHIC_FACTS) }
//
//    @Test
//    fun `starts up`() {
//        appIsStarted()
//
//        aRawScanIsReceived()
//
//        anAvroScanIsForwarded()
//    }
//
//    private fun aRawScanIsReceived() =
//        inputTopic.pipeInput("whatever")
//
//    private fun anAvroScanIsForwarded() =
//        assertThat("Exactly one avro scan should be forwarded", outputTopic.queueSize, `is`(1))
//
//    fun appIsStarted() =
//        application.start()
//}

