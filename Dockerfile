FROM amazoncorretto:11-alpine

WORKDIR /app
COPY ./application/build/install/application /app/

# For example, if you need ports exposing:
# EXPOSE 8080

ENTRYPOINT ["./bin/application"]